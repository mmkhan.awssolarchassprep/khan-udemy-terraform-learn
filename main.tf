provider "aws" {
  region     = "us-west-3"
  access_key = "my-access-key"
  secret_key = "my-secret-key"
}